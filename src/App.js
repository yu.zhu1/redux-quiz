import React, {Component} from 'react';
import './App.less';
import history from "./history";
import { Router} from "react-router-dom";
import {Route, Switch} from "react-router";

import Home from '../src/Home/Home';
import CreateNewNotes from "./Notes/pages/CreateNewNotes";
import ShowExistedNotes from "./Notes/pages/ShowExistedNotes";


class App extends Component{
  render() {
    return (
      <div className='App'>
        <Router history = {history}>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route exact path="/notes/create" component={CreateNewNotes}/>
            <Route exact path="/notes/:id" component={ShowExistedNotes}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;