import React, {Component} from 'react';
import {Link } from 'react-router-dom';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getNotes} from "../Notes/actions/notesActions";
import HeaderNotes from "../Notes/components/HeaderNotes";
import './Home.less';
import NotesList from "../Notes/components/NotesList";

class Home extends Component {


  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    this.props.getNotes();
  }

  render() {

    return (
      <div className='home'>
        <HeaderNotes />
        <NotesList notes = {this.props.notes}/>
      </div>
    );
  }


}

const mapStateToProps = state => ({
  notes: state.notes.notes
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getNotes
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);