import {combineReducers} from "redux";
import notesReducers from "../Notes/reducers/notesReducers";
import createReducer from "../Notes/reducers/createReducer";

const reducers = combineReducers({
    notes:notesReducers,
    newnotes:createReducer
});
export default reducers;