import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class ReturnButton extends Component {
  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div>
        <Link to='/'><button >返回</button></Link>
      </div>
    );
  }
}

export default ReturnButton;