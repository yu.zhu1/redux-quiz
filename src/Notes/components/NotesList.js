import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {MdLibraryAdd} from 'react-icons/md';
import './NotesList.less';

class NotesList extends Component {
  render() {
    return (
      <ul className='notesList'>
        {this.props.notes.map(item => {
            let url = `/notes/${item.id}`;
            return <li><Link to= {url} >{item.title}</Link></li>
          }
        )}
        <li><Link to='/notes/create'><MdLibraryAdd className = 'addIcon'/></Link></li>
      </ul>
    );
  }
}

export default NotesList;