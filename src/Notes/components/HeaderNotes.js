import React, {Component} from 'react';
import './HeaderNotes.less';
import {MdEventNote} from 'react-icons/md';

class HeaderNotes extends Component {
  render() {
    return (
      <div>
        <header>
          <MdEventNote className ='noteIcon'/>
          <h1>NOTES</h1>
        </header>
      </div>
    );
  }
}

export default HeaderNotes;