const initState = {
  notes: {}
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'DELETE_NOTES':
      return {
        ...state,
        notes: action.notes
      };

    default:
      return state
  }
};