const initState = {
  notes: {}
};

export default (state = initState, action) => {
  switch (action.type) {
    case 'CREATE_NEW_NOTE':
      return {
        ...state,
        notes: action.notes
      };

    default:
      return state
  }
};