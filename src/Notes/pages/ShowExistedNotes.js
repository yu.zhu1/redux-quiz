import React, {Component} from 'react';
import {Link} from "react-router-dom";
import HeaderNotes from "../components/HeaderNotes";
import ReturnButton from "../components/ReturnButton";
import "./ShowExistedNotes.less";
import connect from "react-redux/es/connect/connect";
import {deleteNotes} from "../actions/deleteAction";
import history from "../../history";


class ShowExistedNotes extends Component {

  constructor(props, context) {
    super(props, context);
    this.clickDelete = this.clickDelete.bind(this);
  }

  render() {

    return (
      <div>
        <HeaderNotes />
        <div className='notes'>
          <aside>
            <ul>
              {this.props.notes.map(item => {
                let url = `/notes/${item.id}`;
                return <li><Link to={url}>{item.title}</Link></li>
              })}
            </ul>
          </aside>
          <article>
            <h1 >{this.props.notes.find(item =>
              item.id === +this.props.match.params.id
            ).title}</h1>
            <hr/>
            <p className='description'>{this.props.notes.find(item =>
              item.id === +this.props.match.params.id
            ).description}</p>
              <hr/>
            <div className='buttons' >
              <button id='delete' onClick={this.clickDelete}>删除</button>
              <ReturnButton />
            </div>
          </article>
        </div>
      </div>
    );
  }


  clickDelete() {
    deleteNotes(this.props.match.params.id);
    history.push("/");
  }

}

const mapStateToProps = state => ({
  notes: state.notes.notes
});



export default connect(mapStateToProps)(ShowExistedNotes);