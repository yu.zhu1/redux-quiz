import React, {Component} from 'react';
import HeaderNotes from "../components/HeaderNotes";
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux";
import {createNewNotes} from "../actions/createActions";
import {connect} from "react-redux";
import './CreateNewNotes.less';
import history from "../../history";

class CreateNewNotes extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      getTitle:null,
      getContent:null,
      titleIsNull: true,
      contentIsNull: true
    };
    this.changeContent = this.changeContent.bind(this);
    this.changeTitle = this.changeTitle.bind(this);
    this.submitNewNotes = this.submitNewNotes.bind(this);
  }

  render() {

    return (
      <div>
        <HeaderNotes/>
        <div className='newNotes'>
          <h1>创建笔记</h1>
          <hr />
          <form>
            <label id="title">标题</label><br />
            <input onChange={this.changeTitle} /><br/>
            <label id="content">正文</label><br />
            <textarea onChange={this.changeContent} />
          </form>
          <Link to='/'><button id = 'submit' onClick={this.submitNewNotes} disabled={this.state.titleIsNull || this.state.contentIsNull}>提交</button></Link>
          <button id = 'cancel' onClick={this.returnHome} >取消</button>
        </div>
      </div>

    );
  }


  changeTitle(event) {
    if (event.target.value.trim().length !== 0) {
      this.setState({getTitle: event.target.value, titleIsNull: false});
    }
  }

  changeContent(event) {
    if (event.target.value.trim().length !== 0) {
      this.setState({getContent: event.target.value.trim(), contentIsNull: false});
    }
  }

  submitNewNotes() {
      this.props.createNewNotes(this.state.getTitle, this.state.getContent);

  }

  returnHome() {
    history.push("/");
  }
}

const mapStateToProps = state => ({
  notes: state.newnotes.notes
});

const mapDispatchToProps = dispatch => bindActionCreators({
  createNewNotes
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CreateNewNotes);
