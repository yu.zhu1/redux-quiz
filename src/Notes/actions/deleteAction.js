export const deleteNotes = (id) => {
  fetch(`http://localhost:8080/api/posts/${id}`, {
    method: 'DELETE',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify({id: id})
  });
};

