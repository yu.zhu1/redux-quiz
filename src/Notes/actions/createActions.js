export const createNewNotes = (title, description) => (dispatch) => {
  const notes = {title:title, description: description};
  fetch("http://localhost:8080/api/posts", {method:'POST',headers: {'Content-Type': 'application/json'}, body: JSON.stringify(notes)})
    .then(result => {
      console.log(result);
      dispatch({
        type: 'CREATE_NEW_NOTE',
        notes: result
      });
    });

};